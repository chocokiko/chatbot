<?php

    error_log(var_export($_SERVER, true)."\n", 3, "test.log");
    $postdata = json_decode(file_get_contents("php://input"), true);
    error_log(var_export($postdata, true)."\n", 3, "test.log");


    if ($postdata['user_id'] != 'rocket.cat') {
        $message = array(
            'text' => "Response text"
        );
        header('Content-Type: application/json');
        echo json_encode($message);
    }

    /*
        {
          "text": "Response text",
          "attachments": [
            {
              "title": "Rocket.Chat",
              "title_link": "https://rocket.chat",
              "text": "Rocket.Chat, the best open source chat",
              "image_url": "https://rocket.chat/images/mockup.png",
              "color": "#764FA5"
            }
          ]
        }
    */

?>
